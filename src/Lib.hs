module Lib
  ( someFunc) where
import System.Random

data Tree a = Nil | Node a (Tree a) (Tree a)

emptyNode x = x ++ "[label=\"\",style=invis];\n"
middleNode x = x ++ "[label=\"\",style=invis,width=0.1, height=0.1];\n"
node x = x ++ "[label=\"\", shape=circle];\n"
invisEdge x y = x ++ " -- " ++ y ++ "[style=invis];\n"
edge x y = x ++ " -- " ++ y ++ ";\n"


treeToString :: Tree String -> String
treeToString (Node x Nil Nil) = node x
treeToString (Node x (Node y lt rt) Nil) =
  node x ++
  treeToString (Node y lt rt) ++
  middleNode (x++y) ++
  edge x y ++
  invisEdge x (x++y) ++
  emptyNode (x++y++"2") ++
  invisEdge x (x++y++"2")
treeToString (Node x Nil (Node y lt rt) ) =
  node x ++
  emptyNode (x++y) ++
  middleNode (x++y++"2") ++
  invisEdge x (x++y++"2") ++
  treeToString (Node y lt rt) ++
  edge x y ++
  invisEdge x (x++y)
treeToString (Node x (Node y lty rty) (Node z ltz rtz)) =
  node x ++
  treeToString (Node y lty rty) ++
  edge x y ++
  middleNode (x++y++"2") ++
  invisEdge x (x++y++"2") ++
  treeToString (Node z ltz rtz) ++
  edge x z

genTree :: StdGen -> Int -> (Tree String, StdGen)
genTree gen 0 = (Nil, gen)
genTree gen 1 = (Node ("x" ++ (show r)) Nil Nil, newGen)
  where
  (r, newGen) = next gen
genTree gen x =
  (Node rS lt rt, newGenR)
  where
  (r, newGen) = next gen
  rS = "x" ++ show r
  ltx = r `mod` (x-1)
  rtx = x-1-ltx
  (lt, newGenL) = genTree newGen ltx
  (rt, newGenR) = genTree newGenL rtx

someFunc :: IO ()
someFunc = do
  gen <- newStdGen
  let (r, newGen) = next gen
  let (t, _) = genTree newGen ((r `mod` 8) + 8)
  putStrLn $ "graph g {\n " ++ treeToString(t) ++ "\n}"
