#!/bin/sh
mkdir -p outTree
stack build
for i in $(seq 1 6); do
    stack run| dot -Tpdf > outTree/$i.pdf
done
pdfjam outTree/*.pdf --nup 2x3 --no-landscape --outfile out.pdf
rm -rf outTree
