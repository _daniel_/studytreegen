# tree
Generator for binary tree visualizations on pdf.

This is a small, simple tool to generate PDF files with 
visualizations of binary search trees.
Nodes are unlabeled for learning purposes.

This tool is written in haskell and some shell script.

# Requirements
- stack
- pdftk
- graphviz

# usage
Execute `gentree.sh` to generate a pdf file named `out.pdf`:

```bash
./gentree.sh
```

Just look into the shell script to get an idea how it is used.
